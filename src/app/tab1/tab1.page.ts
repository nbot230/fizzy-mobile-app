import {Component, OnInit} from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { ExploreContainerComponent } from '../explore-container/explore-container.component';
import {ClothingService} from "../clothing/clothing.service";
import {NgForOf, NgIf, NgOptimizedImage} from "@angular/common";
import {tap} from "rxjs";
import {CardCatalogueComponent} from "../clothing/components/card-catalogue/card-catalogue.component";

export interface ClothingInterface {
  id: string;
  title: string;
  price: number;
  previousPrice: number;
  caption: string;
  qty: number;
  category: string;
  thumb: string;
  sizes: SizeInterface[];
}

export interface LengthAwarePaginationInterface {
  meta: {
    page: number,
    nextPage: number,
    perPage: number,
    prevPage: number,
    currentPage: number,
    hasMore: boolean
  }
}

export interface SizeInterface {
  id: number;
  value: string;
}

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  standalone: true,
  imports: [IonicModule, ExploreContainerComponent, NgForOf, NgOptimizedImage, NgIf, CardCatalogueComponent],
})
export class Tab1Page implements OnInit {
  clothing: ClothingInterface[] = [];
  clothingPagination: LengthAwarePaginationInterface =  {} as LengthAwarePaginationInterface
  loading: boolean = true;
  error: string | null | undefined = null

  constructor(
    private readonly clothingService: ClothingService
  ) {}

  async ngOnInit(): Promise<void> {
    await this.getClothing(null)
  }

  async getClothing(event: any) {
    this.loading = true;
    try {
      const res = await this.clothingService.getClothing();
      this.clothing = res.clothes;
      this.clothingPagination = res.meta;
    } catch (err: Error | any) {
      console.error(err);

      this.error = err;

    } finally {
      if (!!event) {
        event.target.complete();
      }
      this.loading = false;
    }
  }
}
