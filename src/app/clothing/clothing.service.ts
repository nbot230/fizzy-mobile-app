import {CapacitorHttp, HttpResponse} from '@capacitor/core';
import {BASE_URL} from "../constants/constants";
import {ClothingInterface} from "../tab1/tab1.page";

export class ClothingService {
  public async getClothing(): Promise<{clothes: ClothingInterface[], meta: any}> {
    const res: HttpResponse = await CapacitorHttp.get({
      url: BASE_URL + 'clothes'
    });

    return res.data;
  }
}
