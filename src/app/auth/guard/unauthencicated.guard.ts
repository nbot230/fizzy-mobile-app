import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, tap} from 'rxjs';
import {AuthService} from "../service/auth.service";
import {StorageService} from "../../utils/storage.service";

@Injectable({providedIn: 'root'})
export class UnauthencicatedGuard {
    constructor(
        public authService: AuthService,
        public router: Router,
    ) {}

    async canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Promise<Observable<boolean> | Promise<boolean> | UrlTree | boolean> {
        if (await this.authService.isLoggedIn()) {
            window.alert('Access Denied, Already authenticated!');
            await this.router.navigate(['tabs']);
        }
        return true;
    }
}
