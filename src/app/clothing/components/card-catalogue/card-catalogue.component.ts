import {Component, Input} from "@angular/core";
import {IonicModule} from "@ionic/angular";
import {ExploreContainerComponent} from "../../../explore-container/explore-container.component";
import {NgForOf, NgIf, NgOptimizedImage} from "@angular/common";
import {ClothingInterface, SizeInterface} from "../../../tab1/tab1.page";

@Component({
  selector: 'card-catalogue',
  templateUrl: 'card-catalogue.component.html',
  styleUrls: ['card-catalogue.component.scss'],
  standalone: true,
  imports: [IonicModule, ExploreContainerComponent, NgForOf, NgOptimizedImage, NgIf],
})
export class CardCatalogueComponent {
  @Input() item!: ClothingInterface;

  public extractSizeValue(sizes: SizeInterface[]) {
    return sizes.map(s => s.value).join('/')
  }
}
