import {Routes} from "@angular/router";
import {UnauthencicatedGuard} from "../auth/guard/unauthencicated.guard";

export const routes: Routes = [
    {
        path: 'login',
        canActivate: [UnauthencicatedGuard],
        loadComponent: () =>  import('../page/login/login.page').then((m) => m.LoginPage),
    },
];
