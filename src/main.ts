import { enableProdMode, importProvidersFrom } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { RouteReuseStrategy, provideRouter } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { routes } from './app/app.routes';
import { AppComponent } from './app/app.component';
import { environment } from './environments/environment';
import {ClothingService} from "./app/clothing/clothing.service";
import {HttpClientModule} from "@angular/common/http";
import {StorageService} from "./app/utils/storage.service";
import {AuthService} from "./app/auth/service/auth.service";
import {AuthGuard} from "./app/auth/guard/auth.guard";
import {IonicStorageModule, Storage} from "@ionic/storage-angular";
import {ApiService} from "./app/auth/api/api.service";

if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    ClothingService,
    AuthService,
    AuthGuard,
    Storage,
    StorageService,
    ApiService,
    importProvidersFrom(IonicStorageModule.forRoot({
      name: 'fizzy_secure'
    })),
    importProvidersFrom(IonicModule.forRoot({})),
    importProvidersFrom(HttpClientModule),
    provideRouter(routes),
  ],
});
