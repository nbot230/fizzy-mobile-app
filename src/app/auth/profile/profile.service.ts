import {Injectable} from "@angular/core";
import {CapacitorHttp} from "@capacitor/core";
import {BASE_URL} from "../../constants/constants";

@Injectable()
export class ProfileService {
    public async getProfileData() {
        return CapacitorHttp.get({
            url: BASE_URL + 'profile'
        })
    }
}
