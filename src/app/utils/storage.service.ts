import {Storage} from "@ionic/storage-angular";
import {Injectable} from "@angular/core";

@Injectable({
    providedIn: 'root'
})
export class StorageService {
    private _storage: Storage | null = null;

    initialized = false;

    constructor(
        private readonly storage: Storage
    ) {
        this.init().then(r => {
            console.log('initialized');
            this.initialized = true;
        });
    }

    async init() {
        // If using, define drivers here: await this.storage.defineDriver(/*...*/);
        const storage = await this.storage.create();
        this._storage = storage;
    }

    // Create and expose methods that users of this service can
    // call, for example:
    public async set(key: string, value: any) {
        await this._storage?.set(key, value);
    }

    public async get(key: string): Promise<any> {
        return this._storage?.get(key);
    }

    public async delete(key: string): Promise<void> {
        await this._storage?.remove(key);
    }

    public async getKeys() {
        return this._storage?.keys();
    }
}
