import {Injectable} from "@angular/core";
import {HttpHeaders, HttpOptions, HttpResponse} from "@capacitor/core";
import {Router} from "@angular/router";
import {BASE_URL} from "../../constants/constants";
import {Preferences} from "@capacitor/preferences";
import {AuthService} from "../service/auth.service";

export interface Params {
    [key: string]: string;
}

@Injectable()
export class ApiService {
    constructor(
        private readonly router: Router,
    ) {
    }

    public async getClientData(
        url: string,
        isAuth: boolean = false,
        method: string,
        params: Params
    ) {
        let headers: HttpHeaders = {
            'Content-type': 'application/json'
        };

        if (isAuth) {
            const authToken = await Preferences.get({key: 'user_token'})

            if (!authToken) {
                throw new Error('Auth token not provided for this route');
            }

            headers = Object.assign(headers, {'Authorization': `Bearer ${authToken}`})
        }

        return {
            url: BASE_URL + url,
            responseType: 'json',
            headers
        } as HttpOptions;
    }

    public async handleResponseStatus(res: HttpResponse) {
        if (res.status === 401 || res.status === 403) {
            await Preferences.remove({ key: 'user_token' });
            await this.router.navigate(['auth/login'])
        }
    }
}

export const serialize = function(obj: Params) {
    const str = [];
    for (const p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
}
