import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable, tap} from 'rxjs';
import {AuthService} from "../service/auth.service";
import {StorageService} from "../../utils/storage.service";
import { Preferences } from '@capacitor/preferences';

@Injectable({providedIn: 'root'})
export class AuthGuard {
    constructor(
        public authService: AuthService,
        public router: Router,
    ) {}

    async canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Promise<Observable<boolean> | Promise<boolean> | UrlTree | boolean> {
        if (!await this.authService.isLoggedIn()) {
            window.alert('Access Denied, Login is Required to Access This Page!');
            const res = await this.router.navigate(['auth/login']);
        }
        return true;
    }
}
