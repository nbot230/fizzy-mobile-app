import {Component, EnvironmentInjector, inject} from "@angular/core";
import {IonicModule} from "@ionic/angular";
import {ExploreContainerComponent} from "../../explore-container/explore-container.component";
import {NgForOf, NgIf, NgOptimizedImage} from "@angular/common";
import {CardCatalogueComponent} from "../../clothing/components/card-catalogue/card-catalogue.component";
import {FormControl, FormsModule, ReactiveFormsModule, Validators} from "@angular/forms";
import {MaskitoModule} from "@maskito/angular";
import {MaskitoElementPredicateAsync, MaskitoOptions} from "@maskito/core";
import {AuthService} from "../../auth/service/auth.service";
import { Device } from '@capacitor/device';
import {Router} from "@angular/router";
import {StorageService} from "../../utils/storage.service";

@Component({
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
  standalone: true,
    imports: [
        IonicModule, ExploreContainerComponent, NgForOf, NgOptimizedImage, NgIf,
        FormsModule, MaskitoModule, ReactiveFormsModule,
    ],
})
export class LoginPage {
    isOpen = false;
    openToast = false;
    activeToastData: string = '';

    constructor(
        private readonly auth: AuthService,
        private readonly router: Router,
        private readonly storage: StorageService
    ) {
    }

    readonly phoneMask: MaskitoOptions = {
        mask: ['+', '7', ' ', '(', /\d/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
    };

    readonly codeMask: MaskitoOptions = {
        mask: [/\d/, /\d/, /\d/, /\d/],
    };

    phoneForm = new FormControl('+7 (9', [Validators.required, Validators.pattern('^(\\+7|7|8)?[\\s\\-]?\\(?[489][0-9]{2}\\)?[\\s\\-]?[0-9]{3}[\\s\\-]?[0-9]{2}[\\s\\-]?[0-9]{2}$')])
    codeForm = new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(4)]);
    readonly maskPredicate: MaskitoElementPredicateAsync = async (el) => (el as HTMLIonInputElement).getInputElement();

    public async getCode() {
        const info = await Device.getInfo();

        const phone = this.phoneForm.value as string;
        const res = await this.auth.sendCode({phone: phone.replace(/\D/g, ""), deviceName: info.model});

        if (res.status === 200) {
            this.isOpen = true;
            this.openToast = true;
            this.activeToastData = 'Код отправлен на ваш номер телефона';
            return;
        }

        this.activeToastData = 'Упс, возникла проблема на сервере. Уже решаем!';
        this.openToast = true;
    }

    public async verifyCode(): Promise<void> {
        const info = await Device.getInfo();

        const phone = this.phoneForm.value as string;
        const res = await this.auth.verifyCode({
            code: this.codeForm.value as string,
            phone: phone.replace(/\D/g, ""),
            deviceName: info.model
        });

        if (res.status !== 200) {
            this.activeToastData = 'Неверный код, проверьте сообщения ещё раз :)';
            this.openToast = true;
            this.codeForm.setValue('')
            return;
        }
        await this.auth.storeToken(res.data.token);

        this.isOpen = false;

        await this.router.navigate(['tabs/'])
    }

    onWillDismiss(event: any) {
        console.log(event);

        this.isOpen = false;
    }

    setIsOpen(isOpen: boolean) {
        this.isOpen = isOpen
    }
}
