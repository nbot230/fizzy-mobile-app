import {Injectable} from "@angular/core";
import {CapacitorHttp, HttpOptions, HttpResponse} from '@capacitor/core';
import {StorageService} from "../../utils/storage.service";
import {BASE_URL} from "../../constants/constants";
import {Preferences} from "@capacitor/preferences";
import {ApiService} from "../api/api.service";

export type AuthLoginType = {
    phone: string;
    deviceName: string
};

export type VerifyCodeType = {
    code: string;
} & AuthLoginType;

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(
        private readonly api: ApiService
    ) {
    }
    public async sendCode({phone, deviceName}: AuthLoginType) {
        const verifyCodeData = await this.api.getClientData('login', false, 'POST', {});
        const request = Object.assign(verifyCodeData, {data: {  phone, device_name: deviceName}}) as unknown as HttpOptions;

        return CapacitorHttp.post(request);
    }

    public async verifyCode({phone, code, deviceName}: VerifyCodeType) {
        const verifyCodeData = await this.api.getClientData('verify/code', false, 'POST', {});
        const request = Object.assign(verifyCodeData, {data: {  code, phone, device_name: deviceName}}) as unknown as HttpOptions;

        return CapacitorHttp.post(request);
    }

    public async storeToken(token: string) {
        await Preferences.set({
            key: 'user_token',
            value: token,
        });
    }

    public async isLoggedIn(): Promise<boolean>  {
        const { value } = await Preferences.get({ key: 'user_token' });

        return !!value;
    }
}
